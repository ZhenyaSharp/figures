﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes.Figures
{
    class Triangle
    {
        private Point[] points;
        private Color color;

        public Triangle(Color color)
        {
            this.color = color;

            points = new Point [3];

            points[0].X = 180;
            points[0].Y = 175;
            points[1].X = 300;
            points[1].Y = 240;
            points[2].X = 180;
            points[2].Y = 295;
        }
        

        public void Draw(Graphics graphics)
        {
            graphics.DrawPolygon(new Pen(color, 5),points);
        }
    }
}