﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes.Figures
{
    class Polygon
    {
        private Color color;

        public Polygon(Color color)
        {
            this.color = color;
        }

        public void Draw(Graphics graphics, List<Point> polygonPoints)
        {
            graphics.DrawPolygon(new Pen(color, 5), polygonPoints.ToArray());
        }
    }
}
