﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes.Figures
{
    class Rectangle
    {
        private int x, y, w, h;
        private Color color;

        public Rectangle(int x, int y, int w, int h, Color color)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.color = color;
        }

        public void Draw(Graphics graphics)
        {
            graphics.DrawRectangle(new Pen(color, 5), x, y,w,h);
        }
    }
}
