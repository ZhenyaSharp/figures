﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shapes
{
    class Circle
    {
        private int x, y, d;
        private Color color;

        public Circle(int x, int y, int d, Color color)
        {
            this.x = x;
            this.y = y;
            this.d = d;
            this.color = color;
        }

        public void Draw(Graphics graphics)
        {
            graphics.DrawEllipse(new Pen(color,5), x, y, d, d);
        }

    }
}
