﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shapes
{
    public partial class FormMain : Form
    {
       ShapeManager shapeManager;
       private List<Point> polygonPoints;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            shapeManager=new ShapeManager(pictureBox.Width,pictureBox.Height);
            pictureBox.Image = shapeManager.GetBitmap();
            polygonPoints=new List<Point>();
        }

        private void buttonCircle_Click(object sender, EventArgs e)
        {
            shapeManager.DrawCircle();
            pictureBox.Refresh();
        }

        private void buttonSqare_Click(object sender, EventArgs e)
        {
            shapeManager.DrawSqare();
            pictureBox.Refresh();
        }

        private void buttonTriangle_Click(object sender, EventArgs e)
        {
            shapeManager.DrawTriangle();
            pictureBox.Refresh();
        }

        private void buttonRectangle_Click(object sender, EventArgs e)
        {
            shapeManager.DrawRectangle();
            pictureBox.Refresh();
        }

        private void buttonPolygon_Click(object sender, EventArgs e)
        {
            if (polygonPoints.Count > 2)
            {
               shapeManager.DrawPolygon(polygonPoints);
               pictureBox.Refresh();
            }
            else
            {
                MessageBox.Show("Ошибка! Мало точек для построения фигуры.\nДля добавления точки кликните на рабочую область");
            }
            
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {
            polygonPoints.Add(new Point(e.X, e.Y));
            shapeManager.DrawDot(e.X, e.Y);
            pictureBox.Refresh();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            polygonPoints.Clear();
            shapeManager.ClearGraphics();
            pictureBox.Refresh();
        }
    }
}
