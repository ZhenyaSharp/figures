﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Shapes.Figures;
using Rectangle = Shapes.Figures.Rectangle;

namespace Shapes
{
    class ShapeManager
    {
        private int fieldWidth;
        private int fieldHeight;

        private Circle circle;
        private Sqare sqare;
        private Rectangle rectangle;
        private Triangle triangle;
        private Dot dot;
        private Polygon polygon;

        private Bitmap bitmap;
        private Graphics graphics;


        public ShapeManager(int fieldWidth, int fieldHeight)
        {
            this.fieldWidth = fieldWidth;
            this.fieldHeight = fieldHeight;

            bitmap = new Bitmap(fieldWidth, fieldHeight);
            graphics = Graphics.FromImage(bitmap);

            circle = new Circle(fieldWidth / 2 - 75, fieldHeight / 2 - 75, 150, Color.Black);

            sqare = new Sqare(fieldWidth / 2 - 75, fieldHeight / 2 - 75, 150, Color.Black);

            rectangle = new Rectangle(fieldWidth / 2 - 100, fieldHeight / 2 - 50, 200, 100, Color.Black);

            triangle = new Triangle(Color.Black);

            dot = new Dot(0,0, 5, Color.Black);

            polygon=new Polygon(Color.Black);
        }

        public Bitmap GetBitmap()
        {
            return bitmap;
        }

        public void DrawCircle()
        {
            graphics.Clear(Color.White);
            circle.Draw(graphics);
        }

        public void DrawSqare()
        {
            graphics.Clear(Color.White);
            sqare.Draw(graphics);
        }

        public void DrawTriangle()
        {
            graphics.Clear(Color.White);
            triangle.Draw(graphics);
        }

        public void DrawRectangle()
        {
            graphics.Clear(Color.White);
            rectangle.Draw(graphics);
        }

        public void DrawDot(int x, int y)
        {
            graphics.FillEllipse(new SolidBrush(Color.Black), x, y, 5, 5);
        }

        public void DrawPolygon(List<Point>polygonPoints)
        {
            graphics.Clear(Color.White);
            polygon.Draw(graphics,polygonPoints);
        }

        public void ClearGraphics()
        {
            graphics.Clear(Color.White);
        }
    }
}

